const serverless = require('serverless-http');
const axios = require('axios');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');

// declared in serverless.yml
const USERS_TABLE = process.env.USERS_TABLE;

// check to see if offline or not - 
const IS_OFFLINE = process.env.IS_OFFLINE;
let dynamoDb;
if (IS_OFFLINE === 'true') {
  // for local dev
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  })
} else {
  // for production
  dynamoDb = new AWS.DynamoDB.DocumentClient();
};

app.use(bodyParser.json({ strict: false }));

// simple app.get to test api
app.get('/', function(req, res) {
  res.send('Hello Brandography')
})

// get notification based on ID passed in url 
app.get('/notification/:itemId', function(req, res) {
  const params = {
    TableName: USERS_TABLE,
    Key: {
      itemId: Number(req.params.itemId),
    },
  }
  // DynamoDB function to get notification
  dynamoDb.get(params, (error, results) => {
    if (error) {
      console.log(error);
      res.status(400).json({ error: 'Could not get notification' });
    }
    if (results.Item) {
      // if found, send it
      const notification = results.Item;
      res.json({notification});
    } else {
      // if not found, send 400 
      res.status(400).json({ error: "Notification not found" });
    }
  })
})

// Endpoint to create notification
app.post('/notification', function(req, res) {
  //generate random item ID to track notifications by - used in GET endpoint 
  const itemId = Math.floor(Math.random() * 1000);

  // Search the request body for the following variables and assign them
  const code = req.body.reportError.code;
  const message = req.body.reportError.message;
  const data = req.body.data;

  // create the notification with information from above
  const notification = {
    Item: {
      itemId: itemId,
      reportError: {
        code: code,
        message: message,
      },
      data: data,
    },
  }

//POST to trello function
function postToTrello(data, callback) {
  // Payload - Details for the trello card being created
  const payLoad = { 
    "idList": "5c7eb8a1a6d0a41cbe5092e7",
    "name": "SPS error received ",
    "desc": JSON.stringify(data),
    "idMembers": "5a4264360047f442e4fb7040",
    "keepFromSource": "all"
  };
  // axios POST call to trello
  axios({
    method: 'post',
    url: 'https://api.trello.com/1/cards?key=73029f86014fcb82690aa02e04599c6a&token=f9c5a13ee38b9d61041fe12cc67503528c443cdbaa6a703d2f259b103f07a49b',
    headers: {'Content-Type': 'application/json'},
    //stringify payload so trello can read it
    data: JSON.stringify(payLoad)
  }).then((response) => {
      //send response to the callback
      callback(response);
      return response;
    }).catch((error) => {
      console.log(error, 'error')
      return error;
    })
}
  // Trello Callback to manipulate the reponse from trello to suit our needs
  postToTrello(notification, function(response) {
    // DynmoDb does not accept empty strings, delete empty strings here
    const trelloData = response.data;
    delete trelloData.badges.fogbugz;

    // create the params object that will be saved to the DynmoDb
    const params = {
      TableName: USERS_TABLE,
      Item: {
        itemId: itemId,
        data: {
          trelloData
        },
      },
    }

    // make sure we have the data
    if (!trelloData) {
      // if not - send it back
      res.status(400).send('Trello card was not created.');
    } else {
      // if yes, lets save it to the Db
      saveToDb(params);
    }
  })

  function saveToDb(params) {
    // Save to DynamoDb - params created above
    dynamoDb.put(params, function(err, data) {
      if (err) {
        console.log(err);
        res.status(400).send('dynamoDb put error, could not save notification');
      } else {
        // Return ID w/ success msg on completion
        res.send([itemId, 'success']);
      }
    })
  }
})

module.exports.handler = serverless(app);
